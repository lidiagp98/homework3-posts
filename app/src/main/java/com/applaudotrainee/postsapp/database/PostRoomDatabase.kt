package com.applaudotrainee.postsapp.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.applaudotrainee.postsapp.dao.CommentDao
import com.applaudotrainee.postsapp.dao.PostDao
import com.applaudotrainee.postsapp.dao.UserDao
import com.applaudotrainee.postsapp.model.Comment
import com.applaudotrainee.postsapp.model.Post
import com.applaudotrainee.postsapp.model.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = [User::class, Post::class, Comment::class], version = 1, exportSchema = false)
abstract class PostRoomDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun postDao(): PostDao
    abstract fun commentDao(): CommentDao

    private class PostDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    val userDao = database.userDao()
                    val postDao = database.postDao()
                    val commentDao = database.commentDao()

                    commentDao.deleteAllComments()
                    postDao.deleteAllPosts()
                    userDao.deleteAllUsers()
                }
            }
        }
    }

    companion object {
        @Volatile
        private var INSTANCE: PostRoomDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): PostRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PostRoomDatabase::class.java,
                    "post_database"
                )
                    .addCallback(PostDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                instance
            }
        }

        fun getDatabase(context: Context): PostRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PostRoomDatabase::class.java,
                    "post_database"
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}