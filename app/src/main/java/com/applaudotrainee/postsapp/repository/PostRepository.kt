package com.applaudotrainee.postsapp.repository

import android.database.sqlite.SQLiteConstraintException
import com.applaudotrainee.postsapp.dao.PostDao
import com.applaudotrainee.postsapp.dao.UserDao
import com.applaudotrainee.postsapp.request.DataService
import com.applaudotrainee.postsapp.subset.PostPreview
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

class PostRepository(
    private val postDao: PostDao,
    private val userDao: UserDao
) {

    val postsPreview: Flow<List<PostPreview>> = postDao.getPostsPreview()

    fun getPostComments(postId: Int) = postDao.getPostsWithComments(postId)

    suspend fun updatePosts() {
        withContext(Dispatchers.IO) {
            val posts = DataService.getPosts()
            posts?.let {
                val users = DataService.getUsers(posts)
                userDao.deleteAllUsers()
                userDao.insertUsersList(users)
                try {
                    postDao.insertPostsList(it)
                } catch (e: SQLiteConstraintException) {
                    postDao.insertPostsList(it)
                }
            }
        }
    }
}