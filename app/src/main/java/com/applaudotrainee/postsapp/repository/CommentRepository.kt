package com.applaudotrainee.postsapp.repository

import com.applaudotrainee.postsapp.dao.CommentDao
import com.applaudotrainee.postsapp.request.DataService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CommentRepository(private val commentDao: CommentDao) {

    suspend fun updateComments(postId: Int) {
        withContext(Dispatchers.IO) {
            val comments = DataService.getPostComment(postId)
            comments?.let {
                commentDao.insertCommentsList(it)
            }
        }
    }
}