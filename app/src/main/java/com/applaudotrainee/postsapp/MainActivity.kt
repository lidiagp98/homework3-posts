package com.applaudotrainee.postsapp

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Bundle
import android.util.Log
import android.widget.SearchView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.applaudotrainee.postsapp.adapter.PostPreviewAdapter
import com.applaudotrainee.postsapp.application.PostApplication
import com.applaudotrainee.postsapp.viewmodel.PostViewModel
import com.applaudotrainee.postsapp.viewmodel.PostViewModelFactory
import kotlin.properties.Delegates


class MainActivity : AppCompatActivity() {

    private val postViewModel: PostViewModel by viewModels {
        PostViewModelFactory((application as PostApplication).postRepository)
    }
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    companion object {
        const val POST_IDENTIFIER = "postId"
        const val DEFAULT_POST_VALUE = 1
    }

    object Variables {
        var isNetworkConnected: Boolean by Delegates.observable(false) { property, oldValue, newValue ->
            Log.i("Network connectivity", "$newValue")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        swipeRefreshLayout = findViewById(R.id.swipeLayout)
        val postPreviewRecyclerView = findViewById<RecyclerView>(R.id.postsRecycler)
        startNetworkCallback()
        val postPreviewAdapter = PostPreviewAdapter { postId ->
            if (Variables.isNetworkConnected) {
                val intent = Intent(this, PostWithCommentActivity::class.java).apply {
                    putExtra(POST_IDENTIFIER, postId)
                }
                startActivity(intent)
            } else {
                Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_LONG).show()
            }
        }

        postPreviewRecyclerView.adapter = postPreviewAdapter
        postPreviewRecyclerView.layoutManager = LinearLayoutManager(this)

        if (Variables.isNetworkConnected) postViewModel.insertPosts()
        else Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_LONG)
            .show()

        val searchWidget = findViewById<SearchView>(R.id.searchBar)
        searchWidget.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    postPreviewAdapter.submitList(postViewModel.findPost(query))
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })

        searchWidget.setOnCloseListener {
            postPreviewAdapter.submitList(postViewModel.postsPreview.value)
            false
        }

        postViewModel.postsPreview.observe(this, Observer { posts ->
            posts?.let { postPreviewAdapter.submitList(it) }
            swipeRefreshLayout.isRefreshing = false
        })

        swipeRefreshLayout.setOnRefreshListener {
            if (Variables.isNetworkConnected) postViewModel.insertPosts()
            else {
                swipeRefreshLayout.isRefreshing = false
                Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    private fun startNetworkCallback() {
        val cm: ConnectivityManager =
            application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val builder: NetworkRequest.Builder = NetworkRequest.Builder()

        cm.registerNetworkCallback(
            builder.build(),
            object : ConnectivityManager.NetworkCallback() {

                override fun onAvailable(network: Network) {
                    Variables.isNetworkConnected = true
                }

                override fun onLost(network: Network) {
                    Variables.isNetworkConnected = false
                }
            })
    }


}