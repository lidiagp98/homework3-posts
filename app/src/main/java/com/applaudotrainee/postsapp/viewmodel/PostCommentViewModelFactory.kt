package com.applaudotrainee.postsapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.applaudotrainee.postsapp.repository.CommentRepository
import com.applaudotrainee.postsapp.repository.PostRepository

class PostCommentViewModelFactory(
    private val postRepository: PostRepository,
    private val commentRepository: CommentRepository,
    private val postId: Int
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PostWithCommentViewModel::class.java)) {
            return PostWithCommentViewModel(postRepository, commentRepository, postId) as T
        }
        throw IllegalArgumentException("Unknown View Model")
    }
}