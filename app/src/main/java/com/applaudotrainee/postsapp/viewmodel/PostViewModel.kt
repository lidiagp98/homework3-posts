package com.applaudotrainee.postsapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.applaudotrainee.postsapp.repository.PostRepository
import com.applaudotrainee.postsapp.subset.PostPreview
import kotlinx.coroutines.launch

class PostViewModel(
    private val postRepository: PostRepository
) : ViewModel() {
    val postsPreview = postRepository.postsPreview.asLiveData()

    fun insertPosts() = viewModelScope.launch {
        postRepository.updatePosts()
    }

    fun findPost(search: String): List<PostPreview>? {
        return postsPreview.value?.filter { it.title.contains(search) }
    }

}