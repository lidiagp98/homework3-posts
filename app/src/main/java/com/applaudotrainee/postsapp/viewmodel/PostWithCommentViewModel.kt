package com.applaudotrainee.postsapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.applaudotrainee.postsapp.repository.CommentRepository
import com.applaudotrainee.postsapp.repository.PostRepository
import kotlinx.coroutines.launch

class PostWithCommentViewModel(
    private val postRepository: PostRepository,
    private val commentRepository: CommentRepository,
    private val postId: Int
) : ViewModel() {

    val postComments = postRepository.getPostComments(postId).asLiveData()

    fun insertPostComments() = viewModelScope.launch {
        commentRepository.updateComments(postId)
    }
}