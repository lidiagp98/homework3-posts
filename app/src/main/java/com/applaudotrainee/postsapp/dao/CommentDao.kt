package com.applaudotrainee.postsapp.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudotrainee.postsapp.model.Comment


@Dao
interface CommentDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertComments(vararg comments: Comment)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCommentsList(comments: List<Comment>)

    @Query("DELETE FROM Comment")
    fun deleteAllComments()
}