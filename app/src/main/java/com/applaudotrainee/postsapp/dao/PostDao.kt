package com.applaudotrainee.postsapp.dao

import androidx.room.*
import com.applaudotrainee.postsapp.model.Post
import com.applaudotrainee.postsapp.relationship.PostWithComment
import com.applaudotrainee.postsapp.subset.PostPreview
import kotlinx.coroutines.flow.Flow

@Dao
interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPostsList(posts: List<Post>)

    @Transaction
    @Query("SELECT * FROM Post WHERE postId = :postId")
    fun getPostsWithComments(postId: Int): Flow<PostWithComment>

    @Query("SELECT title, userCreatorId, postId FROM Post")
    fun getPostsPreview(): Flow<List<PostPreview>>

    @Query("DELETE FROM Post")
    fun deleteAllPosts()
}