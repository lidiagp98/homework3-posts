package com.applaudotrainee.postsapp.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudotrainee.postsapp.model.User

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUsersList(users: List<User>)

    @Query("DELETE FROM User")
    fun deleteAllUsers()
}