package com.applaudotrainee.postsapp.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.applaudotrainee.postsapp.database.PostRoomDatabase.Companion.getDatabase
import com.applaudotrainee.postsapp.repository.PostRepository
import retrofit2.HttpException

class RefreshPostDataWorker(appContext: Context, params: WorkerParameters) :
    CoroutineWorker(appContext, params) {

    companion object {
        const val WORK_NAME = "com.applaudotrainee.postapp.work.RefreshPostDataWorker"
        const val WORK_TIME = 15
    }

    override suspend fun doWork(): Result {
        val database = getDatabase(applicationContext)
        val postRepository = PostRepository(database.postDao(), database.userDao())
        try {
            postRepository.updatePosts()
        } catch (e: HttpException) {
            return Result.retry()
        }
        return Result.success()
    }
}