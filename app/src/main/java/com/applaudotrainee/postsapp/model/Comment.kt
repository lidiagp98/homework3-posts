package com.applaudotrainee.postsapp.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(
    foreignKeys = [ForeignKey(
        entity = Post::class,
        parentColumns = ["postId"],
        childColumns = ["commentPostId"],
        onDelete = CASCADE
    )]
)
data class Comment(
    @field:SerializedName("id") @PrimaryKey val commentId: Int,
    @field:SerializedName("postId") val commentPostId: Int,
    @field:SerializedName("name") val name: String,
    @field:SerializedName("email") val email: String,
    @field:SerializedName("body") val body: String
)
