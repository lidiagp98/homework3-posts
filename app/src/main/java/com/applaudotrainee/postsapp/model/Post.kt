package com.applaudotrainee.postsapp.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(
    foreignKeys = [ForeignKey(
        entity = User::class,
        parentColumns = ["userId"],
        childColumns = ["userCreatorId"],
        onDelete = CASCADE
    )]
)
data class Post(
    @field:SerializedName("id") @PrimaryKey val postId: Int,
    @field:SerializedName("userId") val userCreatorId: Int,
    @field:SerializedName("title") val title: String,
    @field:SerializedName("body") val body: String
)
