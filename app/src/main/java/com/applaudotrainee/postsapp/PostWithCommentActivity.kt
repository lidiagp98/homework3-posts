package com.applaudotrainee.postsapp

import android.os.Bundle
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.applaudotrainee.postsapp.adapter.CommentAdapter
import com.applaudotrainee.postsapp.application.PostApplication
import com.applaudotrainee.postsapp.viewmodel.PostCommentViewModelFactory
import com.applaudotrainee.postsapp.viewmodel.PostWithCommentViewModel

class PostWithCommentActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_with_comment)

        val commentRecyclerView = findViewById<RecyclerView>(R.id.commentsRecycler)
        val postTitleTv = findViewById<TextView>(R.id.commentPostTitle)
        val postIdTv = findViewById<TextView>(R.id.commentPostId)
        val postBodyTv = findViewById<TextView>(R.id.commentPostBody)
        val postUserIdTv = findViewById<TextView>(R.id.commentPostUser)

        val postId =
            intent.getIntExtra(MainActivity.POST_IDENTIFIER, MainActivity.DEFAULT_POST_VALUE)
        val postViewModel: PostWithCommentViewModel by viewModels {
            PostCommentViewModelFactory(
                (application as PostApplication).postRepository,
                (application as PostApplication).commentRepository,
                postId
            )
        }
        postViewModel.insertPostComments()
        val commentAdapter = CommentAdapter()
        commentRecyclerView.adapter = commentAdapter
        commentRecyclerView.layoutManager = LinearLayoutManager(this)

        postViewModel.postComments.observe(this, Observer {
            it?.let {
                postIdTv.text = getString(R.string.post_id, it.post.postId)
                postTitleTv.text = it.post.title
                postBodyTv.text = it.post.body
                postUserIdTv.text = getString(R.string.user, it.post.userCreatorId)
                if (it.commentsList.isNullOrEmpty()) {
                    postViewModel.insertPostComments()
                } else {
                    commentAdapter.submitList(it.commentsList)
                }
            }
        })
    }
}