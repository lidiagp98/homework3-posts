package com.applaudotrainee.postsapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.applaudotrainee.postsapp.R
import com.applaudotrainee.postsapp.adapter.PostPreviewAdapter.PostViewHolder
import com.applaudotrainee.postsapp.subset.PostPreview

class PostPreviewAdapter(private val listener: (postId: Int) -> Unit) :
    ListAdapter<PostPreview, PostViewHolder>(PostPreviewComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        return PostViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current, listener)
    }

    class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val postPreviewId: TextView = itemView.findViewById(R.id.postId)
        private val postPreviewTitle: TextView = itemView.findViewById(R.id.postTitle)
        private val postPreviewUserId: TextView = itemView.findViewById(R.id.userId)

        fun bind(postPreview: PostPreview, clickListener: (Int) -> Unit) {
            postPreviewId.text =
                itemView.context.getString(R.string.post_hash_tag, postPreview.postId)
            postPreviewTitle.text = postPreview.title
            postPreviewUserId.text =
                itemView.context.getString(R.string.user_id, postPreview.userCreatorId)
            itemView.setOnClickListener { clickListener(postPreview.postId) }
        }

        companion object {
            fun create(parent: ViewGroup): PostViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.post_preview_item, parent, false)
                return PostViewHolder(view)
            }
        }
    }

    class PostPreviewComparator : DiffUtil.ItemCallback<PostPreview>() {
        override fun areItemsTheSame(oldItem: PostPreview, newItem: PostPreview): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: PostPreview, newItem: PostPreview): Boolean {
            return oldItem.postId == newItem.postId && oldItem.title == newItem.title && oldItem.userCreatorId == newItem.userCreatorId
        }

    }
}


