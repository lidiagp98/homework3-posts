package com.applaudotrainee.postsapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.applaudotrainee.postsapp.R
import com.applaudotrainee.postsapp.model.Comment


class CommentAdapter : ListAdapter<Comment, CommentAdapter.CommentViewHolder>(CommentComparator()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        return CommentViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current.name, current.email, current.body)
    }

    class CommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val commentNameTv: TextView = itemView.findViewById(R.id.commentName)
        private val commentEmailTv: TextView = itemView.findViewById(R.id.commentEmail)
        private val commentBodyTv: TextView = itemView.findViewById(R.id.commentBody)

        fun bind(commentName: String?, commentEmail: String?, commentBody: String) {
            commentNameTv.text = commentName
            commentEmailTv.text = commentEmail
            commentBodyTv.text = commentBody
        }

        companion object {
            fun create(parent: ViewGroup): CommentViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.comments_item, parent, false)
                return CommentViewHolder(view)
            }
        }
    }

    class CommentComparator : DiffUtil.ItemCallback<Comment>() {
        override fun areItemsTheSame(oldItem: Comment, newItem: Comment): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Comment, newItem: Comment): Boolean {
            return oldItem.name == newItem.name &&
                    oldItem.email == newItem.email &&
                    oldItem.body == newItem.body &&
                    oldItem.commentId == newItem.commentId &&
                    oldItem.commentPostId == newItem.commentPostId
        }

    }


}