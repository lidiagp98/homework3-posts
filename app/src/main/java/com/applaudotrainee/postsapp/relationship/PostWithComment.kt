package com.applaudotrainee.postsapp.relationship

import androidx.room.Embedded
import androidx.room.Relation
import com.applaudotrainee.postsapp.model.Comment
import com.applaudotrainee.postsapp.model.Post

data class PostWithComment(
    @Embedded val post: Post,
    @Relation(
        parentColumn = "postId",
        entityColumn = "commentPostId"
    )
    val commentsList: List<Comment>?
)
