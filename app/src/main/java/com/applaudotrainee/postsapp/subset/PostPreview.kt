package com.applaudotrainee.postsapp.subset

data class PostPreview(
    val title: String,
    val userCreatorId: Int,
    val postId: Int
)
