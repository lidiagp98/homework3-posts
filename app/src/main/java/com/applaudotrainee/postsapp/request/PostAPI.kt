package com.applaudotrainee.postsapp.request

import com.applaudotrainee.postsapp.model.Comment
import com.applaudotrainee.postsapp.model.Post
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PostAPI {

    companion object {
        const val POST_URL = "posts"
        const val COMMENT_URL = "comments?"
        const val POST_ID_PARAMETER = "postId"
    }

    @GET(POST_URL)
    fun getPosts(): Call<List<Post>>

    @GET(COMMENT_URL)
    fun getPostComments(@Query(POST_ID_PARAMETER) postId: Int): Call<List<Comment>>
}