package com.applaudotrainee.postsapp.request

import com.applaudotrainee.postsapp.model.Post
import com.applaudotrainee.postsapp.model.User
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.UnknownHostException

object DataService {

    private const val BASE_URL = "https://jsonplaceholder.typicode.com/"

    private val retrofitBuilder: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val postService = retrofitBuilder.create(PostAPI::class.java)

    fun getPosts() = try {
        postService.getPosts().execute().body()
    } catch (e: UnknownHostException) {
        null
    }

    fun getPostComment(postId: Int) = try {
        postService.getPostComments(postId).execute().body()
    } catch (e: UnknownHostException) {
        null
    }

    fun getUsers(posts: List<Post>?): List<User> {
        var users: MutableList<User> = ArrayList()
        posts?.let {
            it.forEach { post ->
                val user = User(post.userCreatorId)
                users.add(user)
            }
        }
        return users
    }
}