package com.applaudotrainee.postsapp.application

import android.app.Application
import androidx.work.*
import com.applaudotrainee.postsapp.database.PostRoomDatabase
import com.applaudotrainee.postsapp.repository.CommentRepository
import com.applaudotrainee.postsapp.repository.PostRepository
import com.applaudotrainee.postsapp.work.RefreshPostDataWorker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

class PostApplication : Application() {
    private val applicationScope = CoroutineScope(SupervisorJob())
    private val postDatabase by lazy { PostRoomDatabase.getDatabase(this, applicationScope) }
    val postRepository by lazy { PostRepository(postDatabase.postDao(), postDatabase.userDao()) }
    val commentRepository by lazy { CommentRepository(postDatabase.commentDao()) }

    private fun setUpUpdateDatabase() {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val repeatingRequest =
            PeriodicWorkRequestBuilder<RefreshPostDataWorker>(
                RefreshPostDataWorker.WORK_TIME.toLong(),
                TimeUnit.MINUTES
            )
                .setConstraints(constraints)
                .build()
        WorkManager.getInstance(applicationContext).enqueueUniquePeriodicWork(
            RefreshPostDataWorker.WORK_NAME,
            ExistingPeriodicWorkPolicy.KEEP,
            repeatingRequest
        )
    }

    private fun delayedInit() {
        applicationScope.launch {
            setUpUpdateDatabase()
        }
    }

    override fun onCreate() {
        super.onCreate()
        delayedInit()
    }
}